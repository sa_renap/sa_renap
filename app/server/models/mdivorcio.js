  var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

    	var divorcio_actaSchema = new Schema({
        unique_codeH : {type: String},
        unique_codeM : {type: String},
        municipio: {type:String},
        lugarDivorcio: {type: String},
        fechaDivorcio : {type: String}
    	});

    module.export  = mongoose.model('acta_divorcio', divorcio_actaSchema);
