var mongoose = require('mongoose'),
Schema = mongoose.Schema;

	var civil_actaSchema = new Schema({
    unique_codeH : {type: String},
    unique_codeM : {type: String},
    municipio: {type:String},
    lugarMatrimonio: {type: String},
    fechaMatrimonio : {type: String},
    regimenMatrimonial : {type: String}
	});

module.export  = mongoose.model('civil_acta', civil_actaSchema);
