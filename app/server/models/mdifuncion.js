var mongoose = require('mongoose'),
Schema = mongoose.Schema;

	var defuncion_actaSchema = new Schema({
    unique_code : {type: String},
    cuiCompareciente : {type: String},
    municipio: {type:String},
    lugarDefuncion: {type: String},
    fechaDefuncion : {type: String},
    causa : {type: String}
	});

module.export  = mongoose.model('defuncion_actas', defuncion_actaSchema);
