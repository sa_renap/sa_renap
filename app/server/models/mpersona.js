var mongoose = require('mongoose'),
	Schema = mongoose.Schema;



	var personaSchema = new Schema({

		descripcion : {type: String},
		nombre: {type: String},
		apellido1: {type: String},
		unique_code : {type: String},
		fechanacimiento : {type: String}, 
		direccion : {type: String},
		//municipio: {type:Schema.Types.ObjectId, ref:'municipio'},
		padres: [{type:Schema.Types.ObjectId , ref:'persona'}]

		
	});

/*

{ 
    "_id" : ObjectId("5a1cad7f80556a8dfcd1e7af"), 
    "nombre" : "Michaell noe", 
    "apellido1" : "Gomez", 
    "apellido2" : "Lopez", 
    "mail" : "erickalisseth9@gmail.com", 
    "image_perfil" : "no_avatar.jpg", 
    "id_socket_online" : "0", 
    "tipo_user" : NumberLong(0), 
    "unique_code" : "yEusJ", 
    "idpersona" : NumberLong(9), 
    "edad" : NumberInt(20), 
    "nacionalidad" : "Guatemala", 
    "estadocivil" : ObjectId("5c173aa624bd810198be7c3f"), 
    "fechanacimiento" : NumberLong(5345353543), 
    "direccion" : "5 av. 3-21 zona 6 ", 
    "genero" : NumberInt(1), 
    "municipio" : ObjectId("5c173b0824bd810198be7c4f"), 
    "padres" : [
        ObjectId("5850702dee838a6cb965a919"), 
        ObjectId("5850702dee838a6cb965a91b")
    ], 
    "estadodefuncion" : ObjectId("5c173d7824bd810198be7c64")
}





    */

module.export  = mongoose.model('persona', personaSchema);
