
var http = require('http');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');
var MongoStore = require('connect-mongo')(session);
var methodOverride  = require("method-override");
var routeDef = express.Router();

var app = express();

app.locals.pretty = true;
app.set('port', process.env.PORT || 8000);
app.set('views', __dirname + '/app/server/views');
app.set('view engine', 'pug');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('stylus').middleware({ src: __dirname + '/app/public' }));
app.use(express.static(__dirname + '/app/public'));
app.use(methodOverride());

// build mongo database connection url //

process.env.DB_HOST = process.env.DB_HOST || '207.154.222.175';
process.env.DB_PORT = process.env.DB_PORT || 27017;
process.env.DB_NAME = process.env.DB_NAME || 'sa_renap';



if (app.get('env') != 'production'){
	process.env.DB_URL = 'mongodb://'+process.env.DB_HOST+':'+process.env.DB_PORT;
}	else {
	//process.env.DB_URL = 'mongodb://'+process.env.DB_USER+':'+process.env.DB_PASS+'@'+process.env.DB_HOST+':'+process.env.DB_PORT;
	process.env.DB_URL = 'mongodb://'+process.env.DB_HOST+':'+process.env.DB_PORT;
}

app.use(session({
	secret: 'faeb4453e5d14fe6f6d04637f78077c76c73d1b4',
	proxy: true,
	resave: true,
	saveUninitialized: true,
	store: new MongoStore({ url: process.env.DB_URL })
	})
);


// Formularios






/*
Definicion de WS-RestFull
*/
var coo = require('./config/dbconn');
var mnacimiento = require('./app/server/models/mpersona');
var cnacimiento = require('./app/server/controllers/cpersona');

var mmatrimonio = require('./app/server/models/mmatrimonio');
var cmatrimonio = require('./app/server/controllers/cmatrimonio');

var mdifuncion = require('./app/server/models/mdifuncion');
var cdifuncion = require('./app/server/controllers/cdifuncion');

var mdivorcio = require('./app/server/models/mdivorcio');
var cdivorcio = require('./app/server/controllers/cdivorcio');


//
routeDef.route('/nacimiento/:id')
    .get(cnacimiento.getPersona);
routeDef.route('/nacimiento/')
	.post(cnacimiento.addPersona);
app.use(routeDef);

routeDef.route('/matrimonio/:id/:bi')
    .get(cmatrimonio.getActa2);
routeDef.route('/matrimonio/')
		.post(cmatrimonio.addMatrimonio);

		routeDef.route('/divorcio/:id/:bi')
		    .get(cdivorcio.getActa3);
		routeDef.route('/divorcio/')
				.post(cdivorcio.addDivorcio);
app.use(routeDef);
/*
routeDef.route('/matrimonio/:id')
    .get(cmatrimonio.getActa2);
routeDef.route('/matrimonio/')
		.post(cmatrimonio.addMatrimonio);
app.use(routeDef);
*/
routeDef.route('/defuncion/:id')
    .get(cdifuncion.getActa);
routeDef.route('/defuncion/')
		.post(cdifuncion.addDefuncion);
app.use(routeDef);


var router = express.Router();
require('./app/server/routes')(router);
app.use(router);









//http.createServer(app).listen(app.get('port'), function(){
//START SERVER en el puerto 8000
app.listen(app.get('port'), () => {
  console.log('App start in PORT ' + app.get('port') + ' Renap');
});

module.exports = app;
