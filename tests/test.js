
'use strict';
process.env.NODE_ENV = 'unit-test';

const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));
const app = require('../app.js');
var shoulds = require('should-http');

describe('API endpoint /', function() {
  this.timeout(5000); // How long to wait for a response (ms)

  it('home page should be up', function(done) {
    chai.request(app)
        .get('/')
        .end(function(err,res){
          res.should.have.status(200);
          done();
        });
    });
  });
