'use strict';
process.env.NODE_ENV = 'unit-test';

const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));
const app = require('../app.js');
var shoulds = require('should-http');


describe('Prueba unitaria: API endpoint /nacimiento/', function() {
  this.timeout(5000); // How long to wait for a response (ms)
  it('Mock: home page esta arriba ', function(done) {
    chai.request(app)
        .get('/')
        .end(function(err,res){
          res.should.have.status(200);
          done();
        });
    });


// POST - Add new
 it('Mock: should Nacimiento return "exito" insert', function() {
   chai.request(app)
       .post('/nacimiento/')
       .send({ "descripcion": "ab48cicj36734","nombre": "ancracio Soft",
       "apellido1": "Soft",
       "direccion": "5 av. 3-21 zona 6",
       "padre": "yEusJ",
       "madre": "yEusJ",
       "fechanacimiento": "02-02-1990"})
       .end(function(err,res){
           res.should.have.status(201);
           res.should.be.json
           res.body.should.have.property('success').and.eql(true);
           res.body.should.have.property('result');
           done();
       });
   });

});
