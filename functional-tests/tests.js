'use strict';
process.env.NODE_ENV = 'test';

const chai = require('chai');
const { Builder, By, Key, until } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const { expect } = require('chai');
const app = require('../app.js');

describe('tests', () => {

    var options   = new chrome.Options();
    options.addArguments('--headless');
    options.addArguments('--no-sandbox');
    const driver = new Builder().forBrowser('chrome')
    .setChromeOptions(options)
    .build();

    it('should go to register page register a new user', async () => {
        await driver.get('http://localhost:8000/signup');
        await driver.sleep(2000);
        await driver.findElement(By.id('name-tf')).sendKeys('Test2');
        await driver.findElement(By.id('email-tf')).sendKeys('test@gmail.com');
        await driver.findElement(By.id('country-list')).findElement(By.xpath('//option[text()="Guatemala"]')).click();
        await driver.findElement(By.id('user-tf')).sendKeys('test2');
        await driver.findElement(By.id('pass-tf')).sendKeys('test123');
        await driver.wait(until.elementLocated(By.id('account-form-btn2')));
        await driver.findElement(By.id('account-form-btn2')).click();
        var element = await driver.findElement(By.xpath('//h4[contains(text(),"Cuenta creada!")]'));

        expect(element).not.to.be.a('null');
    });

    it('should go to login page and login', async () => {
        await driver.get('http://localhost:8000');
        await driver.sleep(2000);
        await driver.findElement(By.id('user-tf')).sendKeys('test2');
        await driver.findElement(By.id('pass-tf')).sendKeys('test123');
        await driver.wait(until.elementLocated(By.xpath('//button[contains(text(),"Entrar")]')));
        await driver.findElement(By.xpath('//button[contains(text(),"Entrar")]')).click();
        const title = await driver.getTitle();

        expect(title).to.equal('Renap 2018 - SA');
    });

    it('should go to home page and delete created user', async () => {
        await driver.get('http://localhost:8000/home');
        await driver.sleep(2000);
        await driver.wait(until.elementLocated(By.id('account-form-btn1')));
        await driver.findElement(By.id('account-form-btn1')).click();
        await driver.sleep(2000);
        await driver.findElement(By.xpath('//div[@class="modal-dialog"]//button[text()="Delete"]')).click();
        var element = await driver.findElement(By.xpath('//h4[contains(text(),"Success")]'));
        expect(element).not.to.be.a('null');
    });

    after(async () => driver.quit());
});
